# Logger Unit Test [Kata][Kata]

## Motivation:
At one point or another, we all wrote code like this

    public class Service {
      private static final Logger LOGGER = LoggerFactor.getLogger(Service.class);
        
      [...]
        
      void info(final String fmt, final Object... arguments) {
        LOGGER.info(fmt, arguments);
      }
    }

We only need one instance for every class, so using a `private static final Logger LOGGER` seems
reasonable. And then comes the moment we write our tests...

## The realization:
The code above is testable if we use a a mocking framework that can mock static methods. Most 
mocking frameworks, like [Mockito][Mockito], cannot mock static methods. Those who can (e.g. 
[PowerMock][PowerMock]), often rely on bytecode manipulation. There is another popular tool that
uses bytecode manipulation: [JaCoCo][JaCoCO]. If those two tools interact, [we will not get a proper
coverage report from JaCoCo (at least not if we use On-the-fly instrumentation)][JaCoCoVSPowerMock],
which can be a show-stopper.

## The Kata:
Goal of this Kata is to rewrite class `Service` in such a way so we can test the call to 
`Service#info(final String fmt, final Object... arguments)` with only JUnit5 and Mockito. You may
add classes and interfaces, as well as modify the constructor of `Service` to your liking. You may
not, however, change the interface `CanLog` or the implementation of this interface within 
`Service`.

Have fun and go wild!

[Kata]: https://en.wikipedia.org/wiki/Kata_(programming)
[Mockito]: https://site.mockito.org
[PowerMock]: https://powermock.github.io
[JaCoCo]: https://www.eclemma.org/jacoco/
[JaCoCoVSPowerMock]: https://github.com/powermock/powermock/wiki/Code-coverage-with-JaCoCo