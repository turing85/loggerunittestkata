package com.turing.loggerunittestkata;

public interface CanLog {
  void info(final String format, final Object... arguments);
}