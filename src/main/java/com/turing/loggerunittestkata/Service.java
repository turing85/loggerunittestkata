package com.turing.loggerunittestkata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Service implements CanLog {
  private final Logger logger = LoggerFactory.getLogger(Service.class);

  @Override
  public void info(final String format, final Object... arguments) {
    logger.info(format, arguments);
  }
}